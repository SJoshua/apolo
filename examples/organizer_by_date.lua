-- This script organizes files inside folder `files` by their access date.
require 'apolo':as_global()

local TMP = core.osname == 'windows' and '%tmp%/apolo_tmp' or '/tmp/apolo_tmp'
local reg_date = '%s(' .. ('[%d/%-]'):rep(10) .. ')%s'

function acc_date(fn)
	local r, d
	if core.osname == 'linux' then
		os.execute('stat "' .. fn .. '" > ' .. TMP)
		r = readf(TMP)
		if r then
			d = r:match('Access:' .. reg_date)
		end
	elseif core.osname == 'windows' then
		os.execute('dir "' .. fn .. '" > ' .. TMP)
		r = readf(TMP)
		if r then
			d = r:match(reg_date)
		end
	end
	if not d then
		return false, 'Not found'
	end
	return d
end

function sort(dir)
	chdir.mk(dir)
	local t = entry_infos(current())
	for fn, arr in pairs(t) do
		if arr.type == 'file' then
			local d = acc_date(fn)
			if d then
				io.write('move "./', dir, '/', fn, '" to "./', dir, '/', d, '/', fn, '"\n')
				mkdir(d)
				move(fn, d)
			else
				io.write('found "./', dir, '/', fn , '"; failed to read its attributes.\n')
			end
		end
	end
end

sort('files')
